package movies

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MoviesApp

fun main(args: Array<String>) {
    runApplication<MoviesApp>(*args)
}

package movies.repositories

import movies.model.Actor
import movies.model.Movie
import org.springframework.data.neo4j.annotation.Query
import org.springframework.data.neo4j.repository.Neo4jRepository

interface MovieRepository : Neo4jRepository<Movie, Long> {
    @Query("""
    MATCH (a:Actor)-[:ACTS_IN]->(m:Movie)
    WITH a, collect(m) AS filmograpy
    WHERE size(filmograpy) >= {0}
    RETURN a
    """)
    fun findActorsByFilmography(minMovies: Long): List<Actor>

    @Query("""
    MATCH (m:Movie)<-[:ACTS_IN]-(a:Actor)
    WHERE a.name STARTS WITH {0}
    RETURN m SKIP {1} LIMIT {2}
    """)
    fun findByActorName(actorName: String, skip: Long, limit: Long): List<Movie>
}
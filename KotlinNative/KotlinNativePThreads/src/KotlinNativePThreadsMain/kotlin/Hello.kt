package limerick.kug

import kotlinx.cinterop.*
import platform.posix.*

fun napTime(microseconds: Int=250) = usleep(microseconds.toUInt() * 1000.toUInt())

fun foobar(input: COpaquePointer?): COpaquePointer? {
    kotlin.native.initRuntimeIfNeeded()
    memScoped {
        val data = input?.asStableRef<Pair<CPointer<ByteVar>, pthread_mutex_t>>()
        val name = data?.get()?.first?.toKString() ?: "No Name"
        val dummy = alloc<pthread_mutex_t>()
        val mutex = data?.get()?.second ?: dummy
        for (x in 1..20) {
            doWork(mutex, name, x)
        }
    }
    return StableRef.create(0).asCPointer()
}

private fun doWork(mutex: pthread_mutex_t, name: String, x: Int) {
    pthread_mutex_lock(mutex.ptr)
    print("Pthread $name message $x part A\n")
    napTime()
    print("Pthread $name message $x part B\n")
    napTime()
    print("Pthread $name message $x part C\n")
    napTime()
    pthread_mutex_unlock(mutex.ptr)
    napTime()
}

fun main() {
    memScoped {
        val mutex = alloc<pthread_mutex_t>()
        pthread_mutex_init(mutex.ptr,null)

        val threadHandle1 = alloc<pthread_tVar>()
        val threadHandle2 = alloc<pthread_tVar>()
        val attrs = null

        val data1 = StableRef.create("Thread No1".cstr.ptr to mutex)
        val data2 = StableRef.create("Thread No2".cstr.ptr to mutex)

        pthread_create(threadHandle1.ptr,
                attrs,
                staticCFunction(::foobar),
                data1.asCPointer())

        pthread_create(threadHandle2.ptr,
                attrs,
                staticCFunction(::foobar),
                data2.asCPointer())

        val result = null
        pthread_join(threadHandle1.value, result)
        pthread_join(threadHandle2.value, result)

        pthread_mutex_destroy(mutex.ptr)
    }

    println("End of main...\n")
}


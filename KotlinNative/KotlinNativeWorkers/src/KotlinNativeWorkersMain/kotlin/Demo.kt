package native.workers

import kotlin.native.concurrent.TransferMode
import kotlin.native.concurrent.Worker

fun main() {
    fun job(name: String): String {
      for(x in 1..20) {
        print("$name message $x\n")
      }
      return "$name Done!"
    }
    val w1 = Worker.start()
    val w2 = Worker.start()

    val future1 = w1.execute(TransferMode.SAFE, {"Worker No1"}, ::job)
    val future2 = w2.execute(TransferMode.SAFE, {"Worker No2"}, ::job)

    println(future1.result)
    println(future2.result)
}
